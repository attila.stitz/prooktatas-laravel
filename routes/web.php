<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Admin')->group(function () { //

  Route::namespace('Auth')->group(function () {
    Route::get('/admin/belepes', 'LoginController@showLoginForm')->name('admin.login.create');
    Route::post('/admin/belepes', 'LoginController@login')->name('admin.login.store');
  });

  Route::middleware('admin_auth')->group(function () {
    Route::get('/admin', 'DashboardController@index')->name('admin.dashboard');
  });
});

Route::middleware('customer_auth')->group(function () {

  Route::get('/ugyfelek', 'CustomersController@index')->name('customers.index');
  Route::delete('/kilepes', 'CustomerAuthController@destroy')->name('login.destroy');

  //opcionalis paraméter

  Route::get('/jegyzetek/uj-jegyzet', 'NotesController@create')->name('notes.create');
  Route::post('/jegyzetek/uj-jegyzet', 'NotesController@store')->name('notes.store');

  Route::get('/jegyzetek/{noteId}/modositas', 'NotesController@edit')->name('notes.edit');
  Route::put('/jegyzetek/{noteId}/modositas', 'NotesController@update')->name('notes.update');

  Route::get('/jegyzetek/{customerId?}', 'NotesController@index')->name('notes.index');

});


Route::get('/', 'PagesController@index')->name('index');

//create, store
Route::get('/regisztracio', 'CustomersController@create')->name('customers.create');
Route::post('/regisztracio', 'CustomersController@store')->name('customers.store');


//edit, update
Route::get('/ugyfel/{customerId}/modositas', 'CustomersController@edit')->name('customers.edit');
Route::put('/ugyfel/{customerId}/modositas', 'CustomersController@update')->name('customers.update');

Route::delete('ugyfel/{customerId}/torles', 'CustomersController@destroy')->name('customers.destroy');

Route::get('/ugyfel/{id}', 'CustomersController@show')->name('customers.show');


Route::get('/belepes', 'CustomerAuthController@create')->name('login.create');

Route::post('/belepes', 'CustomerAuthController@store')->name('login.store');


//  Route::post('/login', 'Auth\LoginController@login')->name('login.store');
//Route::post('/belepes', 'CustomerAuthController@store')->name('login.store');


Route::post('/reg', 'PagesController@register')->name('register');

Route::get('/customer', 'CustomersController@newCustomer')->name('customers.newCustomer');


Route::get('/{page}', 'PagesController@show')->name('pages.show');
