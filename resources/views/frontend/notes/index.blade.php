@extends('frontend.layout.application')

@section('content')
  <h4>Jegyzetek</h4>

  <a href="{{route('notes.create')}}">Új jegyzet létrehozása</a>
<br>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @endif

<form action="{{route('notes.index')}}" method="GET">
  <table border="1">
    <tr>
      <th>Id</th>
      <th>Ügyfél</th>
      <th>Létrehozás dátuma</th>
      <th></th>
      <th></th>
    </tr>
  {{--  <tr>
      <td><input type="text" name="search[id]" value="{{request()->input('search.id')}}"></td>
      <td><input type="text" name="search[name]" value="{{request()->input('search.name')}}"></td>
      <td><input type="text" name="search[email]" value="{{request()->input('search.email')}}"></td>
      <td><input type="text" name="search[updated_at]" value="{{request()->input('search.updated_at')}}"></td>
      <td></td>
      <td></td>
      <td>
        <input type="submit" value="Keresés">
      </td>
    </tr>
--}}
    </tr>
    @foreach($notes as $note)
      <tr>
        <td>{{$note->id}}</td>
        <td>{{$note->customer->name}}</td>
        <td>{{$note->created_at->format('Y-m-d H:i:s')}}</td>
        <td>{{--<a href="{{route('notes.show', ['id' => $note->id])}}">Megtekintés</a>--}}</td>
        <td><a href="{{route('notes.edit', $note->id)}}">Módosítás</a></td>
        <td>
      {{--    <button class="del-button"
            data-token="{{csrf_token()}}"
            data-id="{{$note->id}}"
            data-url="{{route('notes.destroy', $note->id)}}">
            Törlés
          </button>--}}
        {{--  <form action="{{route('notes.destroy', $noteId)}}" method="POST">
            @csrf
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" value="Törlés">
          </form>--}}
        </td>
      </tr>
    @endforeach
  </table>
 </form>
@endsection{{-- @stop--}}
