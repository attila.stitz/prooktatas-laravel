@extends('frontend.layout.application')

@section('content')

  <h4>Jegyzet</h4>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @else
    <form action="{{route('notes.store')}}" method="POST">
      @csrf
      Jegyzet:
      <textarea name="content">{{old('content')}}</textarea>
      <br><br>
      Cimkék:
      {{--több válasz esetén tömbként küldjük tovább--}}
      <select name="tags[]" multiple>
        @foreach($tags as $tag)
          <option value="{{$tag->id}}">{{$tag->name}}</option>
        @endforeach
      </select>

      <button type="submit">Mentés</button>
    </form>
  @endif

@stop
