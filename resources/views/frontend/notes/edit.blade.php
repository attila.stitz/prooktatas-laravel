@extends('frontend.layout.application')

@section('content')

  <h4>Jegyzetek</h4>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @else
    <form action="{{route('notes.update', $note->id)}}" method="POST">
      <input type="hidden" name="_method" value="PUT">
      @csrf
      Jegyzet:
      <textarea name="content">{{old('content') ?: $note->content}}</textarea>
      <br><br>
      Cimkék:
      {{--több válasz esetén tömbként küldjük tovább--}}
      <select name="tags[]" multiple>
        @foreach($tags as $tag)
          <option value="{{$tag->id}}" {{$note->hasTag($tag->id) ? 'selected' : ''}}>{{$tag->name}}</option>
        @endforeach
      </select>

      <button type="submit">Mentés</button>
    </form>
  @endif

@stop
{{-- {{in_array($tag->id, old('tags', []) )}}--}}
