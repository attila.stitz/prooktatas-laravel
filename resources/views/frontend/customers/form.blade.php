<form action="{{$customer->id ? route('customers.update', $customer->id) : route('customers.store')}}" method="POST">
  @if($customer->id)
    <input type="hidden" name="_method" value="PUT">{{--PATCH--}}
  @endif
  @csrf
  Név:
  {{--{{old('name') ? old('name') : $customer->name}}--}}
  <input type="text" name="name" value="{{old('name') ?: $customer->name}}">
  {{--@if(old('name'))
    {{old('name')}}
  @else
    {{$customer->name}}
  @endif
  --}}
  @if($errors->first('name'))
    <p style="color:red;">
      {{$errors->first('name')}}
    </p>
  @endif
  Email:
  <input type="text" name="email" value="{{old('email') ?: $customer->email}}">
  @if($errors->first('email'))
    <p style="color:red;">
      {{$errors->first('email')}}
    </p>
  @endif
  <br>
  Jelszó:
  <input type="password" name="password">
  <br>
  Jelszó mégegyszer:
  <input type="password" name="password_confirmation">
  @if($errors->first('password'))
    <p style="color:red;">
      {{$errors->first('password')}}
    </p>
  @endif
  @if(!$customer->id)
    <br>
    <label>
      <input type="checkbox" name="terms" value="1" {{old('terms') ? 'checked' : ''}}>
      Elfogadok mindent
    </label>
  @endif
    <br>
  <input type="submit" value="Mentés">
</form>
