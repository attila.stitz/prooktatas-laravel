<a href="{{route('index')}}">Kezdőlap</a> |

<a href="{{route('customers.create')}}">Regisztráció |

  <a href="{{route('pages.show', ['page' => 'contact'])}}">Kontakt</a> |
  <a href="{{route('pages.show', ['page' => 'gallery'])}}">Galéria</a> |
  {{-- \Auth::  ugyanaz mint  auth()->--}}
  @if(auth()->guard('customer')->check())
        <a href="{{route('customers.index')}}">Ügyfelek</a> |

    Belépve: {{ auth()->guard('customer')->user()->name }}

    <form action="{{route('login.destroy')}}" method="POST">
      @csrf
      <input type="hidden" name="_method" value="DELETE">
      <input type="submit" value="Kilépés">
    </form>

  @endif
