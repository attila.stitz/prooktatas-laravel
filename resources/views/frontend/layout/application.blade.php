<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  @include('frontend.layout.head')
  <body>
    @include('frontend.layout.menu')
    @yield('content')

    @yield('footer')

    @include('frontend.layout.scripts')
    <script>
        @isset($initOrder)
        $(document).ready(function(){
            //initSortable({name:'desc',email:'asc'});
            initSortable({!!$initOrder!!});

        });
        @endisset
    </script>
  </body>
</html>
