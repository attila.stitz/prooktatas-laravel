@extends('frontend.layout.application')

@section('content')
  <h4>Valami</h4>


<form action="{{route('register')}}" method="POST">
  @csrf
  Név:
  <input type="text" name="name" value="{{old('name')}}">
  @if($errors->first('name'))
    <p style="color:red;">
      {{$errors->first('name')}}
    </p>
  @endif
  <br>
  Jelszó:
  <input type="password" name="password">
  <br>
  Jelszó mégegyszer:
  <input type="password" name="password_confirmation">

  @if($errors->first('password'))
    <p style="color:red;">
      {{$errors->first('password')}}
    </p>
  @endif
  <br>
  <input type="submit" value="Register">
</form>

@stop

@section('footer')
<footer>
  footer
</footer>
@stop
