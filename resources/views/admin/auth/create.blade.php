@extends('admin.layout.admin-layout')

@section('content')

  <h4>Belépés</h4>

  @if($errors->first('email'))
    <h2 style="color:red">{{$errors->first('email')}}</h2>
  @endif

  <form action="{{route('admin.login.store')}}" method="POST">
    @csrf
    Email:
    <input type="text" name="email" value="{{old('email')}}">
    <br>
    Password:
    <input type="password" name="password">
    <br><br>
    <button type="submit">Belépés</button>
  </form>

@stop
