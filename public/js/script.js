$(document).ready(function(){
  $('.del-button').click(function() {
    var token = $(this).data('token');
    var url = $(this).data('url');
    var customerId = $(this).data('id');
    if(url !== undefined) {
      $.ajax({
         url: url,
         type: 'DELETE',
         data: { '_token': token},
         dataType:'json',
         success: function(data){
           if(data.message) {
             $('#customer-'+customerId).remove();
             alert(data.message);
           }
         }
      //  dataType: 'mycustomtype'
      });
    }
  });

  window.initSortable=function(sortBy){
      console.log(sortBy);
      $('th[data-sortable]').each(function (){

        let sortName=$(this).data('sort-name');
        let sortClass='fa-sort';
        let sortOrder='';
        /*
          <i className="fas fa-sort"></i>
          <i className="fas fa-sort-alpha-down"></i>
          <i className="fas fa-sort-alpha-up-alt"></i>
*/
          if(sortBy[$(this).data('sort-name')]){
              sortOrder=sortBy[$(this).data('sort-name')];
          }
        switch (sortOrder){
            case 'asc':
                sortClass+='-alpha-down';
                break;
            case 'desc':
                sortClass+='-alpha-up-alt';
                break;
        }
        $(this).append('<i class="sortIcon fas '+sortClass+'"></i>' +
            '<input type="hidden" name="order['+sortName+']" value="'+sortOrder+'">');

      });
      $('.sortIcon').click(function (){
        ///console.log($(this).parent().data());
          let sortName=$(this).parent().data('sort-name');
          let sortOrder=$(this).parent().find('input').val();
          //console.log(sortName);
          let newOrder='asc';
          switch (sortOrder){
              case 'asc':
                  newOrder='desc';
                  break;
              case 'desc':
                  newOrder='';
                  break;
          }
          /*
          console.log(sortName);
          console.log(sortOrder);
          console.log(newOrder);
           */
          $("input[name='order["+sortName+"]']").val(newOrder);
          $('form').submit();
            //$("input[name='order[name]']")
      });

  }


});

