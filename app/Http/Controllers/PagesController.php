<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
      $i = rand(1,5);
      $b = 'asd';

      return view('index')
        ->with('i', $i)
        ->with('b', $b);
    }


    public function show(Request $request, $page)
    {
      dd($page);
      dd($request->all());


      return view('frontend.pages.show')
            ->with('page', $page);
    }


    public function register(Request $request)
    {
      $rules = ['name' => 'required', 'password' => 'required|confirmed'];

      $this->validate($request, $rules);

      return redirect()->route('index');

    }
}
