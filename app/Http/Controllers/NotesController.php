<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use App\Models\Customer;
use Carbon\Carbon;
use App\Models\Tag;

class NotesController extends Controller
{
                        //ha nincs megadva, akkor null
  public function index($customerId = null)
  {
    $notes = Note::all();

    if($customerId) {
        $customer = Customer::find($customerId);
        //$notes = $customer->notes()->get();
        $notes = $customer->notes;
        //dd($notes);

        $now = Carbon::now();
        //csak mai jegyzetek.       //whereDate: elég ha a dátum egyezik, az időpont részt figyelmen kívül hagyja.
        $notes = $customer->notes()->whereDate('created_at', '=', $now)->orderBy('created_at', 'desc')->get();
    }
//    $note = Note::first();
//    dd($note->customer()->first()); ////kapcsolódó customer objektumot
  //  dd($note->customer);    //kapcsolódó customer objektumot

    return view('frontend.notes.index')->with('notes', $notes);
  }

  public function create()
  {                 //ha nincs megadva másik paraméter az orderBy()nál akkor növekvőbe rendez.
    $tags = Tag::orderBy('name')->get();

    return view('frontend.notes.create')->with('tags', $tags);
  }


  public function store(Request $request)
  {
      $note = new Note;
      $note->content = $request->input('content');
      $note->customer_id = auth()->guard('customer')->id();// ugyanaz, mint  $note->customer()->associate(auth()->guard('customer')->id())

      $note->save();


  //$note->tags()->attach([1,2,4]);  hozzárendeli a $notehoz a 1,2,4 id-jú tag-eket.
                              //[1,2,4]
                              //$note->tags()->attach([1 => ['weight' => 3, 'extrafield' => 'kutya']]);
      $note->tags()->attach($request->input('tags'));

      //$note->tags()->detach([2,3]);

      return redirect()->route('notes.index');
  }

  public function edit($noteId)
  {
    $note = Note::find($noteId);
    $tags = Tag::orderBy('name')->get();

    return view('frontend.notes.edit')
    ->with('note', $note)
    ->with('tags', $tags);
  }

  public function update(Request $request, $noteId)
  {
      $note = Note::find($noteId);
      $note->content = $request->input('content');
      $note->save();

      $note->tags()->sync($request->input('tags'));

//      $note->tags()->detach(); //mindent detachol
//      $note->tags()->attach($request->input('tags'));

      return redirect()->route('notes.index');

  }

}
