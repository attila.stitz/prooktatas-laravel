<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Note extends Model
{

  public function getCustomerName()
  {
      $customer = Customer::find($this->customer_id);

      return $customer->name;
  }

  public function hasTag($tagId)
  {
                  //tömbként a tagek IDját
  //  return in_array($tagId, $this->tags()->pluck('id'));

  //  return $this->tags()->where('id', $tagId)->count();
    return $this->tags()->find($tagId);
  }

  public function customer()
  { //1-többhöz kapcsolat, 'customer_id'  idegen kulcsal.
  //  return $this->belongsTo(Customer::class/*, 'customer_id'*/);/*Default elnevezésnél nem kell megadni*/
    return $this->belongsTo(Customer::class);
  }

  public function tags()
  {
    return $this->belongsToMany(Tag::class)->withTimestamps();//->withPivot(['weight', 'sajt']);
  }

}
