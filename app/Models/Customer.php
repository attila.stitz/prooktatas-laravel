<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class Customer extends Authenticatable
{
    //public $table = 'customer_data';


    public function lastUpdated()
    {
      return $this->updated_at->format('Y-m-d');
    }

    public function notes()
    {
        //1 a többhöz kapcsolat
      return $this->hasMany(Note::class);
    }

    public function setAttributes($data)
    {
      $this->name = $data['name'];
      $this->email = $data['email'];
/*if($data['password'])    //ha nem üres string */

      if(isset($data['password']) && $data['password']) {
        $this->password = \Hash::make($data['password']);
      }
    }

    public function scopeSearch($query, $data)
    {
      if(isset($data['name']) && $data['name']) {   //$search['name']  = Horvath;
        $query->where('name', 'LIKE', '%'.$data['name'].'%');
      }

      if(isset($data['email']) && $data['email']) {   //$search['name']  = Horvath;
        $query->where('email', 'LIKE', '%'.$data['email'].'%');
      }

      //Customer::where('name', 'LIKE', 'valami')->orWhere('email', 'LIKE', 'másvalami');
    }

    public function scopeSearchOrder($query, $data)
    {
        //dd($data);
        if(isset($data['name']) && $data['name']) {
            //dd($data);
            $query->orderBy('name',$data['name'] );
        }

        if(isset($data['email']) && $data['email']) {
            $query->orderBy('email',$data['email'] );
        }
        //Customer::where('name', 'LIKE', 'valami')->orWhere('email', 'LIKE', 'másvalami');
    }

    public function scopeFreshRegister($query)
    {
      //a héten regisztráltak
      $date = Carbon::now()->subWeek()->format('Y-m-d');
      $query->where('created_at', '>', $date);
    }
}
