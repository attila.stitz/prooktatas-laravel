<?php

use App\Models\Customer;
use App\Models\Note;

use Illuminate\Database\Seeder;

class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Note::truncate();
        $faker = Faker\Factory::create('Hu_hu');
        $tagNumber=\App\Models\Tag::count();
        for ($i = 0; $i < rand(10,15); $i++) {
            $note = new Note;
            $note->content = $faker->realText(rand(20,100));
            $note->customer_id= Customer::inRandomOrder()->first()->id;
            $note->save();
            $tags=[];
            echo $note->id.PHP_EOL;
            for ($ti = 0; $ti <= rand(0,$tagNumber); $ti++) {
                $tagId=\App\Models\Tag::inRandomOrder()->first()->id;
                $tags[]=$tagId;
                echo ' tags#'.$tagId;
            }
            echo PHP_EOL;
            $note->tags()->attach($tags);
        }


    }
}
