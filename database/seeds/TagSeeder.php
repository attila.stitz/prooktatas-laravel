<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $tag = new Tag;
      $tag->name = 'Cimke #1';
      $tag->save();

      $tag = new Tag;
      $tag->name = 'Cimke #2';
      $tag->save();

      $tag = new Tag;
      $tag->name = 'Cimke #3';
      $tag->save();

        //factory(Tag::class, 10)->create();
        $faker = Faker\Factory::create('hu_HU');
        $words=$faker->unique()->words(10);
        foreach ($words as $word){
            $tag = new Tag;
            $tag->name = $word;
            $tag->save();
        }
    }
}
